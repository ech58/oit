# Duke Office of Information Technology Chatbot 

## What is this?
This chatbot is an instance of IBM Watson Assistant (Plus Plan). It helps answer Duke OIT-related questions, including Net-ID passwords, Duke Blue, Multi-factor Authentification, Email, ePrint, Duke Card, Sakai, Zoom, Duke Hub, Qualtrics and more. 

![Test Website](/images/TestWebsite.png "Test Website")

## Requirements
- Sign up for an IBM Cloud account: https://cloud.ibm.com/registration
- Install Docker: https://docs.docker.com/engine/install/ubuntu/
- Install Docker Compose: https://docs.docker.com/compose/install/
- It is recommended to configure a secure webhook URL with Nginx.

## Infrastructure
Here is a diagram that illustrates the infrastructure of the chatbot:

![Infrastructure Diagram](/images/infrastructure.svg "Infrastructure Diagram")

After a user sends a message, the chatbot will detect an intent that triggers one of its dialog nodes. This prompts the chatbot to call a webhook URL to an IBM Cloud Function, which grabs the JSON object containing the user's input and passes it to the flask app. (The Cloud Function is vital to this setup because it ensures the transfer of information from Watson Assistant. Directly calling the Flask app from a dialog node causes an error in production, where the chatbot is unresponsive and unable to write to a database.) The Cloud Function sends this JSON object to the Flask app via a POST request, routing first through an Nginx docker container, which provides a secure web server for the Flask app.

The dockerized Flask app performs three major functions:
1. Renders an HTML template with the embedded chatbot.
2. Parses the JSON object sent from the Cloud Function and writes the content into a dockerized MySQL database.
3. Displays the contents of the MySQL database.

The Flask app also contains code that can be used for performing CRUD operations on the MySQL database.

## How to Build

Build the docker containers (Flask app & MySQL) with the command: 

`sudo docker-compose up`

Build the docker containers (Flask app & MySQL) to persist after you close your terminal with the command:

`sudo docker-compose up -d`

After you update any flask files, make sure to run the command before building the containers:

`sudo docker-compose build app`

## How to access
Get a preview of the OIT Chatbot: https://web-chat.global.assistant.watson.cloud.ibm.com/preview.html?region=us-south&integrationID=5189ba89-db94-4297-a9c5-5cc3678190fe&serviceInstanceID=6aeb977f-2814-47fd-83e1-3c261acf5aaf

Go to the test website with the embedded bot: https://chatbot-test-03.oit.duke.edu/

## Challenge
We tried two approaches to extract conversation data: 1. Using a **post-message** webhook under Assistant settings, you can obtain a JSON object that contains the detected intent, the intent detection confidence score, and the chatbot's response. 2. Enabling a webhook on **each dialog node** and passing in the user's input as a parameter, you can obtain a JSON object with only the user's question. However, manually toggle this setting for each dialog node is a lot of work, and formatting issues arise with newline characters and multi-turn logic. In the end, we decided to use the second approach, because the user's question is the most important piece of information. 

## Next Steps
- Technically, you could obtain all the conversation data by making a Watson Assistant API request and parsing the returned JSON object: https://cloud.ibm.com/apidocs/assistant/assistant-v2#message. 

- For the purpose of security, you would need to restrict access to the database and prevent sensitive data from storing into the database.

- Finally, the pricing of IBM Watson Assistant is based on the number of Monthly Active Users, you should consider implementing browser cookies to prevent double counting the users. Under IBM's pricing model, a user who clears their cookies will appear unique with each browser visit, so this is something worth taking into consideration.
