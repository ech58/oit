from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/")
def my_template():
    return render_template('OIT-IBM.html')
