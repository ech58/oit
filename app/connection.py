import mysql.connector
from mysql.connector import Error
import sys 

def create_server_connection(host_name, user_name, user_password, db_name):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password, 
            database=db_name
        )
        print("MySQL Database connection successful", file=sys.stderr)
    except Error as err:
        print(f"Error: '{err}'",  file=sys.stderr)

    return connection

#uses the connection.commit() method to make 
#sure that the commands detailed in our SQL queries are implemented.
def execute_query(connection, query, data):
    print("Start of execute query :)", file=sys.stderr)
    cursor = connection.cursor()
    try:
        cursor.execute(query, data)
        connection.commit()
        print("Query successful",  file=sys.stderr)
    except Error as err:
        print(f"Error: '{err}'",  file=sys.stderr)