CREATE DATABASE oit_qna;
use oit_qna;

CREATE TABLE questions (
  question VARCHAR(1024),
  intent VARCHAR(1024)
);

INSERT INTO questions
  (question, intent)
VALUES
  ('Tell me a little bit about Warpwire', 'Warpwire'),
  ('How do I install VPN?', 'VPN_Install');
