import xml.etree.ElementTree as ET

#parsing & getting the root
tree = ET.parse('chat 20210120 v4.xml')
root = tree.getroot()

mySet = set()
myDict = dict()
#take out some tags
for child in root:
    if(child.tag=='sys_attachment'):
        root.remove(child)
    if(child.tag=='sys_attachment_doc'):
        root.remove(child)

    #get the names of the staff members
    if(child.tag=='chat_queue_entry'):
        for subchild in child:
            if(subchild.tag=='assigned_to'):
                if subchild.attrib.get('display_value') not in myDict.keys():
                    myDict[subchild.attrib.get('display_value')] = 1
                else:
                    myDict[subchild.attrib.get('display_value')] += 1
    
    if(child.tag=='sys_journal_field'):
        for subchild in child:
            if(subchild.tag=='sys_created_by'):
                if subchild.text not in myDict.keys():
                    myDict[subchild.text] = 1
                else:
                    myDict[subchild.text] += 1

    #eliminate DHTS
    if(child.tag=='chat_queue_entry'):
        queue_value = ''
        assignment_group_value = ''
        if(child.find('queue')!=None):
            queue_value = child.find('queue').attrib.get('display_value')
        if(child.find('assignment_group')!=None):
            assignment_group_value = child.find('assignment_group').attrib.get('display_value')
        if (queue_value=='Service Desk (DHTS)'):
            root.remove(child)
    
    for subchild in child:
        if(subchild.tag=='sys_id'):
            child.remove(subchild)
        if(subchild.tag=='action'):
            child.remove(subchild)
        if(subchild.tag=='active'):
            child.remove(subchild)
        if(subchild.tag=='activity_due'):
            child.remove(subchild)
        if(subchild.tag=='additional_assignee_list'):
            child.remove(subchild)
        if(subchild.tag=='agile_story'):
            child.remove(subchild)
        if(subchild.tag=='approval'):
            child.remove(subchild)
        if(subchild.tag=='approval_history'):
            child.remove(subchild)
        if(subchild.tag=='approval_set'):
            child.remove(subchild)
        if(subchild.tag=='business_service'):
            child.remove(subchild)
        if(subchild.tag=='chat_channel'):
            child.remove(subchild)
        if(subchild.tag=='close_notes'):
            child.remove(subchild)
        if(subchild.tag=='cmdb_ci'):
            child.remove(subchild)
        if(subchild.tag=='comments'):
            child.remove(subchild)
        if(subchild.tag=='comments_and_work_notes'):
            child.remove(subchild)
        if(subchild.tag=='company'):
            child.remove(subchild)
        if(subchild.tag=='name'):
            child.remove(subchild)

        #take out big data objects
        if(subchild.tag=='data'):
            child.remove(subchild)
        if(subchild.tag=='element'):
            child.remove(subchild)
        if(subchild.tag=='element_id'):
            child.remove(subchild)
        
        #take out tags with None text
        if(subchild.tag=='work_notes'):
            child.remove(subchild)
        if(subchild.tag=='document_id'):
            child.remove(subchild)
        if(subchild.tag=='service_offering'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_short_http_referer'):
            child.remove(subchild)
        if(subchild.tag=='location'):
            child.remove(subchild)
        if(subchild.tag=='u_it_service'):
            child.remove(subchild)
        if(subchild.tag=='from_table'):
            child.remove(subchild)
        if(subchild.tag=='skills'):
            child.remove(subchild)
        if(subchild.tag=='document_table'):
            child.remove(subchild)
        if(subchild.tag=='rejection_goto'):
            child.remove(subchild)
        if(subchild.tag=='parent'):
            child.remove(subchild)
        if(subchild.tag=='calendar_duration'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_remote_addr'):
            child.remove(subchild)
        if(subchild.tag=='sla_due'):
            child.remove(subchild)
        if(subchild.tag=='u_vendor_reference'):
            child.remove(subchild)
        if(subchild.tag=='work_notes_list'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_http_user_agent'):
            child.remove(subchild)
        if(subchild.tag=='u_customer_segments'):
            child.remove(subchild)
        if(subchild.tag=='delivery_task'):
            child.remove(subchild)
        if(subchild.tag=='expected_start'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_script_name'):
            child.remove(subchild)
        if(subchild.tag=='u_service_provider'):
            child.remove(subchild)
        if(subchild.tag=='work_end'):
            child.remove(subchild)
        if(subchild.tag=='u_application'):
            child.remove(subchild)
        if(subchild.tag=='image_height'):
            child.remove(subchild)
        if(subchild.tag=='due_date'):
            child.remove(subchild)
        if(subchild.tag=='u_business_service'):
            child.remove(subchild)
        if(subchild.tag=='u_converted_from'):
            child.remove(subchild)
        if(subchild.tag=='u_chat_queue_entry'):
            child.remove(subchild)
        if(subchild.tag=='correlation_id'):
            child.remove(subchild)
        if(subchild.tag=='from_id'):
            child.remove(subchild)
        if(subchild.tag=='owner_id'):
            child.remove(subchild)
        if(subchild.tag=='image_width'):
            child.remove(subchild)
        if(subchild.tag=='wf_activity'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_server_name'):
            child.remove(subchild)
        if(subchild.tag=='variables'):
            child.remove(subchild)
        if(subchild.tag=='u_converted_to'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_ess_group'):
            child.remove(subchild)
        if(subchild.tag=='follow_up'):
            child.remove(subchild)
        if(subchild.tag=='average_image_color'):
            child.remove(subchild)
        if(subchild.tag=='u_technical_service'):
            child.remove(subchild)
        if(subchild.tag=='order'):
            child.remove(subchild)
        if(subchild.tag=='watch_list'):
            child.remove(subchild)
        if(subchild.tag=='group_list'):
            child.remove(subchild)
        if(subchild.tag=='u_last_work_note'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_http_referer'):
            child.remove(subchild)
        if(subchild.tag=='u_sei_to_remove'):
            child.remove(subchild)
        if(subchild.tag=='time_worked'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_short_script_name'):
            child.remove(subchild)
        if(subchild.tag=='correlation_display'):
            child.remove(subchild)
        if(subchild.tag=='owner_table'):
            child.remove(subchild)
        if(subchild.tag=='delivery_plan'):
            child.remove(subchild)
        if(subchild.tag=='user_input'):
            child.remove(subchild)
        if(subchild.tag=='u_perl_api_user'):
            child.remove(subchild)
        if(subchild.tag=='business_duration'):
            child.remove(subchild)
        if(subchild.tag=='u_sei_text'):
            child.remove(subchild)

        #remove tags that only have true / false as their values
        if(subchild.text=='true'):
            child.remove(subchild)
        
#for everything in mySet:
    #print(everything)

#sort the dictionary from most occurrence to least occurrence
myDict = sorted(myDict.items(), key=lambda x: x[1], reverse=True)
for key, value in myDict:
    if value<=50:
        mySet.add(key)

for child in root:
    if(child.tag=='sys_journal_field'):
        for subchild in child:
            if(subchild.tag=='sys_created_by'):
                if subchild.text in mySet:
                    print(child.find('value').text)

    if(child.tag=='chat_queue_entry'):
        for subchild in child:
            if(subchild.tag=='assigned_to'):
                if subchild.attrib.get('display_value') in mySet:
                    print(child.find('short_description').text)
                    
#write new file 
#tree.write('output.xml')
